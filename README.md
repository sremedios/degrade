# Degrade

This is a small library to degrade (blur and downsample) a signal. Here, we
aim to properly model the forward process in signal acquisition.
